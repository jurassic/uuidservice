package com.uuidservice.ws.dao.base;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.uuidservice.ws.BaseException;
import com.uuidservice.ws.core.GUID;
import com.uuidservice.ws.dao.FiveTenDAO;
import com.uuidservice.ws.data.FiveTenDataObject;
import com.uuidservice.ws.service.DAOFactoryManager;

import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;

public class DefaultFiveTenDAOTest
{
    private final LocalServiceTestHelper helper =
        new LocalServiceTestHelper(new LocalDatastoreServiceTestConfig());

    @Before
    public void setUp() throws Exception {
        helper.setUp();
    }

    @After
    public void tearDown() throws Exception {
        helper.tearDown();
    }

    private List<String> createTestFiveTens(int count) throws BaseException
    {
        System.out.println(">>>>> Begin: createTestFiveTens() with count = " + count);
        List<String> guids = new ArrayList<String>(count);
        for(int i = 0; i < count; i++) {
            //String name = "test fiveTens " + (i+1);
            FiveTenDataObject dObj = new FiveTenDataObject();   // TBD
            String guid = DAOFactoryManager.getDAOFactory().getFiveTenDAO().createFiveTen(dObj);
            System.out.println("guid = " + guid);
            guids.add(guid);
        }
        System.out.println("<<<<< End: createTestFiveTens().");
        return guids;
    }

    private String createTestFiveTen() throws BaseException
    {
        List<String> guids = createTestFiveTens(1);
        return guids.get(0);
    }
    
    @Test
    public void testGetFiveTen()
    {
        System.out.println(">>>>>>>>>> Begin: testGetFiveTen()");
        try {
            String guid1 = createTestFiveTen();

            FiveTenDataObject dObj = DAOFactoryManager.getDAOFactory().getFiveTenDAO().getFiveTen(guid1);
            assertNotNull(dObj);

            String guid2 = dObj.getGuid();
            System.out.println("getFiveTen(): guid2 = " + guid2);
            assertEquals(guid1, guid2);
        } catch(BaseException ex) {
            fail("Failed to retrieve fiveTen object.");
            ex.printStackTrace();
        }
    }

    @Test
    public void testGetAllFiveTens()
    {
        System.out.println(">>>>>>>>>> Begin: testGetAllFiveTens()");
        try {
            int count = 5;
            List<String> guids = createTestFiveTens(count);
            
            List<FiveTenDataObject> dObjs = DAOFactoryManager.getDAOFactory().getFiveTenDAO().getAllFiveTens();
            assertNotNull(dObjs);

            Iterator<FiveTenDataObject> it = dObjs.iterator();
            while(it.hasNext()) {
                FiveTenDataObject dObj = (FiveTenDataObject) it.next();
                System.out.println("dObj = " + dObj);
            }
            
            int count2 = dObjs.size();
            assertEquals(count, count2);
        } catch(BaseException ex) {
            fail("Failed to retrieve fiveTen object list.");
            ex.printStackTrace();
        }
    }

    @Test
    public void testFindFiveTens()
    {
        System.out.println(">>>>>>>>>> Begin: testFindFiveTens()");
        try {
            int count = 5;
            List<String> guids = createTestFiveTens(count);
            
            String filter = "";
            //String ordering = "name asc, createdTime desc";
            String ordering = "createdTime desc";
            String params = "";
            List<String> values = new ArrayList<String>();
            values.add("");
            
            List<FiveTenDataObject> dObjs = DAOFactoryManager.getDAOFactory().getFiveTenDAO().findFiveTens(filter, ordering, params, values);
            assertNotNull(dObjs);
            
            Iterator<FiveTenDataObject> it = dObjs.iterator();
            while(it.hasNext()) {
                FiveTenDataObject dObj = (FiveTenDataObject) it.next();
                System.out.println("dObj = " + dObj);
            }
            int count2 = dObjs.size();
            System.out.println("count2 = " + count2);
            assertEquals(count, count2);
            
            //filter = "name == 'xxx'";
            //ordering = "name asc, createdTime desc";
            ordering = "createdTime desc";
            params = "";
            values = new ArrayList<String>();
            values.add("");
            
            dObjs = DAOFactoryManager.getDAOFactory().getFiveTenDAO().findFiveTens(filter, ordering, params, values);
            assertNotNull(dObjs);
            
            it = dObjs.iterator();
            while(it.hasNext()) {
                FiveTenDataObject dObj = (FiveTenDataObject) it.next();
                System.out.println("dObj = " + dObj);
            }
            count2 = dObjs.size();
            System.out.println("count2 = " + count2);
            //assertEquals(0, count2);
            
            //filter = "name == 'test fiveTen 1'";
            //ordering = "name asc, createdTime desc";
            ordering = "createdTime desc";
            params = "";
            values = new ArrayList<String>();
            values.add("");
            
            dObjs = DAOFactoryManager.getDAOFactory().getFiveTenDAO().findFiveTens(filter, ordering, params, values);
            assertNotNull(dObjs);
            
            it = dObjs.iterator();
            while(it.hasNext()) {
                FiveTenDataObject dObj = (FiveTenDataObject) it.next();
                System.out.println("dObj = " + dObj);
            }
            count2 = dObjs.size();
            System.out.println("count2 = " + count2);
            //assertEquals(1, count2);
            
            //filter = "name == nameParam";
            //ordering = "name asc, createdTime desc";
            ordering = "createdTime desc";
            //params = "String nameParam";
            values = new ArrayList<String>();
            //values.add("abc");
            
            dObjs = DAOFactoryManager.getDAOFactory().getFiveTenDAO().findFiveTens(filter, ordering, params, values);
            assertNotNull(dObjs);
            
            it = dObjs.iterator();
            while(it.hasNext()) {
                FiveTenDataObject dObj = (FiveTenDataObject) it.next();
                System.out.println("dObj = " + dObj);
            }
            count2 = dObjs.size();
            System.out.println("count2 = " + count2);
            //assertEquals(0, count2);
            
            //filter = "name == nameParam";
            //ordering = "name asc, createdTime desc";
            ordering = "createdTime desc";
            //params = "String nameParam";
            values = new ArrayList<String>();
            //values.add("test fiveTen 1");
            
            dObjs = DAOFactoryManager.getDAOFactory().getFiveTenDAO().findFiveTens(filter, ordering, params, values);
            assertNotNull(dObjs);
            
            it = dObjs.iterator();
            while(it.hasNext()) {
                FiveTenDataObject dObj = (FiveTenDataObject) it.next();
                System.out.println("dObj = " + dObj);
            }
            count2 = dObjs.size();
            System.out.println("count2 = " + count2);
            //assertEquals(1, count2);
            
            //filter = "name == nameParam1 || name == nameParam2";
            //ordering = "name asc, createdTime desc";
            ordering = "createdTime desc";
            //params = "String nameParam1, String nameParam2";
            values = new ArrayList<String>();
            //values.add("test fiveTen 1");
            //values.add("test fiveTen 2");
            
            dObjs = DAOFactoryManager.getDAOFactory().getFiveTenDAO().findFiveTens(filter, ordering, params, values);
            assertNotNull(dObjs);
            
            it = dObjs.iterator();
            while(it.hasNext()) {
                FiveTenDataObject dObj = (FiveTenDataObject) it.next();
                System.out.println("dObj = " + dObj);
            }
            count2 = dObjs.size();
            System.out.println("count2 = " + count2);
            //assertEquals(2, count2);

        } catch(BaseException ex) {
            fail("Failed to retrieve fiveTen object list.");
            ex.printStackTrace();
        }
    }

    @Test
    public void testCreateFiveTen()
    {
        System.out.println(">>>>>>>>>> Begin: testCreateFiveTen()");
        try {
            String guid1 = GUID.generate();
            //String name = "test fiveTen 1";
            FiveTenDataObject dObj = new FiveTenDataObject(guid1);   // TBD
            System.out.println("guid1 = " + guid1);

            String guid2 = DAOFactoryManager.getDAOFactory().getFiveTenDAO().createFiveTen(dObj);
            System.out.println("guid2 = " + guid2);

            assertEquals(guid1, guid2);
        } catch(BaseException ex) {
            fail("Failed to create a fiveTen object.");
            ex.printStackTrace();
        }
    }

    @Test
    public void testUpdateFiveTen()
    {
        System.out.println(">>>>>>>>>> Begin: testUpdateFiveTen()");
        try {
            //String name1 = "test fiveTen name 1";
            FiveTenDataObject dObj1 = new FiveTenDataObject();   // TBD
            String guid = DAOFactoryManager.getDAOFactory().getFiveTenDAO().createFiveTen(dObj1);
            System.out.println("guid = " + guid);
            
            // String name2 = "test fiveTen name 2";
            FiveTenDataObject dObj2 = new FiveTenDataObject();   // TBD
            Boolean suc = DAOFactoryManager.getDAOFactory().getFiveTenDAO().updateFiveTen(dObj2);
            assertTrue(suc);
            
            FiveTenDataObject dObj3 = DAOFactoryManager.getDAOFactory().getFiveTenDAO().getFiveTen(guid);
            assertNotNull(dObj3);
            
            String guid3 = dObj3.getGuid();
            assertEquals(guid, guid3);
        } catch(BaseException ex) {
            fail("Failed to create a fiveTen object.");
            ex.printStackTrace();
        }
    }

    @Test
    public void testDeleteFiveTen()
    {
        System.out.println(">>>>>>>>>> Begin: testDeleteFiveTen()");
        try {
            String guid1 = createTestFiveTen();

            Boolean suc = DAOFactoryManager.getDAOFactory().getFiveTenDAO().deleteFiveTen(guid1);
            assertTrue(suc);
        } catch(BaseException ex) {
            fail("Failed to retrieve fiveTen object.");
            ex.printStackTrace();
        }
    }

}
