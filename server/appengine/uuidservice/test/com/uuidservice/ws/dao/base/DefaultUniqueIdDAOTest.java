package com.uuidservice.ws.dao.base;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.uuidservice.ws.BaseException;
import com.uuidservice.ws.core.GUID;
import com.uuidservice.ws.dao.UniqueIdDAO;
import com.uuidservice.ws.data.UniqueIdDataObject;
import com.uuidservice.ws.service.DAOFactoryManager;

import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;

public class DefaultUniqueIdDAOTest
{
    private final LocalServiceTestHelper helper =
        new LocalServiceTestHelper(new LocalDatastoreServiceTestConfig());

    @Before
    public void setUp() throws Exception {
        helper.setUp();
    }

    @After
    public void tearDown() throws Exception {
        helper.tearDown();
    }

    private List<String> createTestUniqueIds(int count) throws BaseException
    {
        System.out.println(">>>>> Begin: createTestUniqueIds() with count = " + count);
        List<String> guids = new ArrayList<String>(count);
        for(int i = 0; i < count; i++) {
            //String name = "test uniqueIds " + (i+1);
            UniqueIdDataObject dObj = new UniqueIdDataObject();   // TBD
            String guid = DAOFactoryManager.getDAOFactory().getUniqueIdDAO().createUniqueId(dObj);
            System.out.println("guid = " + guid);
            guids.add(guid);
        }
        System.out.println("<<<<< End: createTestUniqueIds().");
        return guids;
    }

    private String createTestUniqueId() throws BaseException
    {
        List<String> guids = createTestUniqueIds(1);
        return guids.get(0);
    }
    
    @Test
    public void testGetUniqueId()
    {
        System.out.println(">>>>>>>>>> Begin: testGetUniqueId()");
        try {
            String guid1 = createTestUniqueId();

            UniqueIdDataObject dObj = DAOFactoryManager.getDAOFactory().getUniqueIdDAO().getUniqueId(guid1);
            assertNotNull(dObj);

            String guid2 = dObj.getGuid();
            System.out.println("getUniqueId(): guid2 = " + guid2);
            assertEquals(guid1, guid2);
        } catch(BaseException ex) {
            fail("Failed to retrieve uniqueId object.");
            ex.printStackTrace();
        }
    }

    @Test
    public void testGetAllUniqueIds()
    {
        System.out.println(">>>>>>>>>> Begin: testGetAllUniqueIds()");
        try {
            int count = 5;
            List<String> guids = createTestUniqueIds(count);
            
            List<UniqueIdDataObject> dObjs = DAOFactoryManager.getDAOFactory().getUniqueIdDAO().getAllUniqueIds();
            assertNotNull(dObjs);

            Iterator<UniqueIdDataObject> it = dObjs.iterator();
            while(it.hasNext()) {
                UniqueIdDataObject dObj = (UniqueIdDataObject) it.next();
                System.out.println("dObj = " + dObj);
            }
            
            int count2 = dObjs.size();
            assertEquals(count, count2);
        } catch(BaseException ex) {
            fail("Failed to retrieve uniqueId object list.");
            ex.printStackTrace();
        }
    }

    @Test
    public void testFindUniqueIds()
    {
        System.out.println(">>>>>>>>>> Begin: testFindUniqueIds()");
        try {
            int count = 5;
            List<String> guids = createTestUniqueIds(count);
            
            String filter = "";
            //String ordering = "name asc, createdTime desc";
            String ordering = "createdTime desc";
            String params = "";
            List<String> values = new ArrayList<String>();
            values.add("");
            
            List<UniqueIdDataObject> dObjs = DAOFactoryManager.getDAOFactory().getUniqueIdDAO().findUniqueIds(filter, ordering, params, values);
            assertNotNull(dObjs);
            
            Iterator<UniqueIdDataObject> it = dObjs.iterator();
            while(it.hasNext()) {
                UniqueIdDataObject dObj = (UniqueIdDataObject) it.next();
                System.out.println("dObj = " + dObj);
            }
            int count2 = dObjs.size();
            System.out.println("count2 = " + count2);
            assertEquals(count, count2);
            
            //filter = "name == 'xxx'";
            //ordering = "name asc, createdTime desc";
            ordering = "createdTime desc";
            params = "";
            values = new ArrayList<String>();
            values.add("");
            
            dObjs = DAOFactoryManager.getDAOFactory().getUniqueIdDAO().findUniqueIds(filter, ordering, params, values);
            assertNotNull(dObjs);
            
            it = dObjs.iterator();
            while(it.hasNext()) {
                UniqueIdDataObject dObj = (UniqueIdDataObject) it.next();
                System.out.println("dObj = " + dObj);
            }
            count2 = dObjs.size();
            System.out.println("count2 = " + count2);
            //assertEquals(0, count2);
            
            //filter = "name == 'test uniqueId 1'";
            //ordering = "name asc, createdTime desc";
            ordering = "createdTime desc";
            params = "";
            values = new ArrayList<String>();
            values.add("");
            
            dObjs = DAOFactoryManager.getDAOFactory().getUniqueIdDAO().findUniqueIds(filter, ordering, params, values);
            assertNotNull(dObjs);
            
            it = dObjs.iterator();
            while(it.hasNext()) {
                UniqueIdDataObject dObj = (UniqueIdDataObject) it.next();
                System.out.println("dObj = " + dObj);
            }
            count2 = dObjs.size();
            System.out.println("count2 = " + count2);
            //assertEquals(1, count2);
            
            //filter = "name == nameParam";
            //ordering = "name asc, createdTime desc";
            ordering = "createdTime desc";
            //params = "String nameParam";
            values = new ArrayList<String>();
            //values.add("abc");
            
            dObjs = DAOFactoryManager.getDAOFactory().getUniqueIdDAO().findUniqueIds(filter, ordering, params, values);
            assertNotNull(dObjs);
            
            it = dObjs.iterator();
            while(it.hasNext()) {
                UniqueIdDataObject dObj = (UniqueIdDataObject) it.next();
                System.out.println("dObj = " + dObj);
            }
            count2 = dObjs.size();
            System.out.println("count2 = " + count2);
            //assertEquals(0, count2);
            
            //filter = "name == nameParam";
            //ordering = "name asc, createdTime desc";
            ordering = "createdTime desc";
            //params = "String nameParam";
            values = new ArrayList<String>();
            //values.add("test uniqueId 1");
            
            dObjs = DAOFactoryManager.getDAOFactory().getUniqueIdDAO().findUniqueIds(filter, ordering, params, values);
            assertNotNull(dObjs);
            
            it = dObjs.iterator();
            while(it.hasNext()) {
                UniqueIdDataObject dObj = (UniqueIdDataObject) it.next();
                System.out.println("dObj = " + dObj);
            }
            count2 = dObjs.size();
            System.out.println("count2 = " + count2);
            //assertEquals(1, count2);
            
            //filter = "name == nameParam1 || name == nameParam2";
            //ordering = "name asc, createdTime desc";
            ordering = "createdTime desc";
            //params = "String nameParam1, String nameParam2";
            values = new ArrayList<String>();
            //values.add("test uniqueId 1");
            //values.add("test uniqueId 2");
            
            dObjs = DAOFactoryManager.getDAOFactory().getUniqueIdDAO().findUniqueIds(filter, ordering, params, values);
            assertNotNull(dObjs);
            
            it = dObjs.iterator();
            while(it.hasNext()) {
                UniqueIdDataObject dObj = (UniqueIdDataObject) it.next();
                System.out.println("dObj = " + dObj);
            }
            count2 = dObjs.size();
            System.out.println("count2 = " + count2);
            //assertEquals(2, count2);

        } catch(BaseException ex) {
            fail("Failed to retrieve uniqueId object list.");
            ex.printStackTrace();
        }
    }

    @Test
    public void testCreateUniqueId()
    {
        System.out.println(">>>>>>>>>> Begin: testCreateUniqueId()");
        try {
            String guid1 = GUID.generate();
            //String name = "test uniqueId 1";
            UniqueIdDataObject dObj = new UniqueIdDataObject(guid1);   // TBD
            System.out.println("guid1 = " + guid1);

            String guid2 = DAOFactoryManager.getDAOFactory().getUniqueIdDAO().createUniqueId(dObj);
            System.out.println("guid2 = " + guid2);

            assertEquals(guid1, guid2);
        } catch(BaseException ex) {
            fail("Failed to create a uniqueId object.");
            ex.printStackTrace();
        }
    }

    @Test
    public void testUpdateUniqueId()
    {
        System.out.println(">>>>>>>>>> Begin: testUpdateUniqueId()");
        try {
            //String name1 = "test uniqueId name 1";
            UniqueIdDataObject dObj1 = new UniqueIdDataObject();   // TBD
            String guid = DAOFactoryManager.getDAOFactory().getUniqueIdDAO().createUniqueId(dObj1);
            System.out.println("guid = " + guid);
            
            // String name2 = "test uniqueId name 2";
            UniqueIdDataObject dObj2 = new UniqueIdDataObject();   // TBD
            Boolean suc = DAOFactoryManager.getDAOFactory().getUniqueIdDAO().updateUniqueId(dObj2);
            assertTrue(suc);
            
            UniqueIdDataObject dObj3 = DAOFactoryManager.getDAOFactory().getUniqueIdDAO().getUniqueId(guid);
            assertNotNull(dObj3);
            
            String guid3 = dObj3.getGuid();
            assertEquals(guid, guid3);
        } catch(BaseException ex) {
            fail("Failed to create a uniqueId object.");
            ex.printStackTrace();
        }
    }

    @Test
    public void testDeleteUniqueId()
    {
        System.out.println(">>>>>>>>>> Begin: testDeleteUniqueId()");
        try {
            String guid1 = createTestUniqueId();

            Boolean suc = DAOFactoryManager.getDAOFactory().getUniqueIdDAO().deleteUniqueId(guid1);
            assertTrue(suc);
        } catch(BaseException ex) {
            fail("Failed to retrieve uniqueId object.");
            ex.printStackTrace();
        }
    }

}
