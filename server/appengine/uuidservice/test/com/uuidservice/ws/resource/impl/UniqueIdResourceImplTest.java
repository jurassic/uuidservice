package com.uuidservice.ws.resource.impl;

import static org.junit.Assert.fail;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Iterator;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.uuidservice.ws.UniqueId;
import com.uuidservice.ws.stub.ErrorStub;
import com.uuidservice.ws.stub.UniqueIdStub;
import com.uuidservice.ws.stub.UniqueIdListStub;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.ClientResponse.Status;
import com.sun.jersey.api.client.WebResource;

public class UniqueIdResourceImplTest
{
    private final static String BASE_URI = "http://localhost:8888/v1";

    // WS client.
    private Client wsClient = null;
    
    private void printClientResponseInfo(ClientResponse clientResponse)
    {
        int status = clientResponse.getStatus();
        System.out.println("Status = " + status);
        MultivaluedMap<String,String> headers = clientResponse.getHeaders();
        Iterator<MultivaluedMap.Entry<String,List<String>>> it = headers.entrySet().iterator();
        while(it.hasNext()) {
            MultivaluedMap.Entry<String,List<String>> m =(MultivaluedMap.Entry<String,List<String>>) it.next();
            String k = (String) m.getKey();
            List<String> l = (m.getValue() == null) ? new ArrayList<String>() : m.getValue(); // ???
            System.out.println("Header key = " + k);
            for(String v : l) {
                System.out.println("       value = " + v);                    
            }
        }
    }

    @Before
    public void setUp() throws Exception
    {
        //ClientConfig config = new DefaultClientConfig();
        //config.getClasses().add(JSONRootElementProvider.class);
        //wsClient = Client.create(config);
        //wsClient.addFilter(new LoggingFilter());

        wsClient = Client.create();
    }

    @After
    public void tearDown() throws Exception
    {
        //wsClient.destroy();
    }

    @Test
    public void testGetAllUniqueIds()
    {
        WebResource webResource = wsClient.resource(BASE_URI);
        ClientResponse clientResponse = webResource.path("uniqueIds").path("all").accept(MediaType.APPLICATION_XML).get(ClientResponse.class);
        
        printClientResponseInfo(clientResponse);

        if(clientResponse.getStatus() == Status.OK.getStatusCode()) {
            UniqueIdListStub stub = clientResponse.getEntity(UniqueIdListStub.class);
            System.out.println("UniqueId list stub = " + stub);
        } else {
            ErrorStub errorStub = clientResponse.getEntity(ErrorStub.class);
            System.out.println("Error = " + errorStub);
        }
    }

    @Test
    public void testFindUniqueIds()
    {
        fail("Not yet implemented");
    }

    @Test
    public void testGetUniqueId()
    {
        //String guid = "fake_guid";
        //String guid = "123abc";
        String guid = "11e6ba6c-a003-40f4-851e-03035d9dbac4";
        WebResource webResource = wsClient.resource(BASE_URI);
        //ClientResponse clientResponse = webResource.path("uniqueIds").path(guid).accept(MediaType.TEXT_PLAIN).get(ClientResponse.class);
        ClientResponse clientResponse = webResource.path("uniqueIds").path(guid).accept(MediaType.APPLICATION_XML).get(ClientResponse.class);
        //ClientResponse clientResponse = webResource.path("uniqueIds").path(guid).accept(MediaType.APPLICATION_JSON).get(ClientResponse.class);

        printClientResponseInfo(clientResponse);

        if(clientResponse.getStatus() == Status.OK.getStatusCode()) {
            UniqueIdStub stub = clientResponse.getEntity(UniqueIdStub.class);
            System.out.println("UniqueId stub = " + stub);
        } else {
            ErrorStub errorStub = clientResponse.getEntity(ErrorStub.class);
            System.out.println("Error = " + errorStub);
            //String message = clientResponse.getEntity(String.class);
            //System.out.println("Error message = " + message);
        }
    }

    @Test
    public void testGetUniqueIdWithField()
    {
        //String guid = "fake_guid";
        //String guid = "123abc";
        String guid = "11e6ba6c-a003-40f4-851e-03035d9dbac4";
        //String field = "name";
        String field = "createdTime";
        WebResource webResource = wsClient.resource(BASE_URI);
        ClientResponse clientResponse = webResource.path("uniqueIds").path(guid).path(field).accept(MediaType.TEXT_PLAIN).get(ClientResponse.class);
        //ClientResponse clientResponse = webResource.path("uniqueIds").path(guid).path(field).accept(MediaType.APPLICATION_XML).get(ClientResponse.class);
        //ClientResponse clientResponse = webResource.path("uniqueIds").path(guid).path(field).accept(MediaType.APPLICATION_JSON).get(ClientResponse.class);

        printClientResponseInfo(clientResponse);

        if(clientResponse.getStatus() == Status.OK.getStatusCode()) {
            String obj = clientResponse.getEntity(String.class);
            System.out.println("Object = " + obj);
        } else {
            String errorMsg = clientResponse.getEntity(String.class);
            System.out.println("ErrorMsg = " + errorMsg);
        }
    }

    @Test
    public void testCreateUniqueIdStub()
    {
        UniqueId bean = new UniqueId() {
            private String guid;
            private String name;
            private String requesterUserAgent;
            private String requesterHostname;
            private String requesterIpAddress;
            private Long createdTime;
            private Long modifiedTime;

            @Override
            public String getGuid()
            {
                return guid;
            }

            @Override
            public String getName()
            {
                return name;
            }

            @Override
            public String getRequesterUserAgent()
            {
                return requesterUserAgent;
            }

            @Override
            public String getRequesterHostname()
            {
                return requesterHostname;
            }

            @Override
            public String getRequesterIpAddress()
            {
                return requesterIpAddress;
            }

            @Override
            public Long getCreatedTime()
            {
                return createdTime;
            }

            @Override
            public Long getModifiedTime()
            {
                return modifiedTime;
            }

        };
        
        
        UniqueIdStub stub = new UniqueIdStub(bean);
        //UniqueIdStub stub = new UniqueIdStub();
        //UniqueIdStub stub = null;
        
        WebResource webResource = wsClient.resource(BASE_URI);
        //ClientResponse clientResponse = webResource.path("uniqueIds").entity(stub, MediaType.APPLICATION_XML).post(ClientResponse.class);
        ClientResponse clientResponse = webResource.path("uniqueIds").accept(MediaType.APPLICATION_XML).entity(stub, MediaType.APPLICATION_XML).post(ClientResponse.class);

        printClientResponseInfo(clientResponse);

        if(clientResponse.getStatus() == Status.CREATED.getStatusCode()) {
            String guid = clientResponse.getEntity(String.class);
            System.out.println("New uniqueId guid = " + guid);
        } else {
            ErrorStub errorStub = clientResponse.getEntity(ErrorStub.class);
            System.out.println("Error = " + errorStub);
            //String message = clientResponse.getEntity(String.class);
            //System.out.println("Error message = " + message);
        }
    }

    @Test
    public void testUpdateUniqueIdStringUniqueIdStub()
    {
        final String uniqueIdGuid = "4e87c81523559c184a280628d82704d860f31474";
        
        UniqueId bean = new UniqueId() {
            private String guid;
            private String name;
            private String requesterUserAgent;
            private String requesterHostname;
            private String requesterIpAddress;
            private Long createdTime;
            private Long modifiedTime;

            @Override
            public String getGuid()
            {
                return guid;
            }

            @Override
            public String getName()
            {
                return name;
            }

            @Override
            public String getRequesterUserAgent()
            {
                return requesterUserAgent;
            }

            @Override
            public String getRequesterHostname()
            {
                return requesterHostname;
            }

            @Override
            public String getRequesterIpAddress()
            {
                return requesterIpAddress;
            }

            @Override
            public Long getCreatedTime()
            {
                return createdTime;
            }

            @Override
            public Long getModifiedTime()
            {
                return modifiedTime;
            }

        };
        
        UniqueIdStub stub = new UniqueIdStub(bean);
        //UniqueIdStub stub = new UniqueIdStub();
        //UniqueIdStub stub = null;
        
        WebResource webResource = wsClient.resource(BASE_URI);
        ClientResponse clientResponse = webResource.path("uniqueIds").path(uniqueIdGuid).entity(stub, MediaType.APPLICATION_XML).put(ClientResponse.class);

        printClientResponseInfo(clientResponse);

        if(clientResponse.getStatus() == Status.NO_CONTENT.getStatusCode()) {
            System.out.println("UniqueId successfully updated.");
        } else {
            ErrorStub errorStub = clientResponse.getEntity(ErrorStub.class);
            System.out.println("Error = " + errorStub);
            //String message = clientResponse.getEntity(String.class);
            //System.out.println("Error message = " + message);
        }
    }

    @Test
    public void testDeleteUniqueId()
    {
        //String guid = "123abc";
        String guid = "d87a09c94b1c701ee98a7b6ade062c50c2d26d82";
        
        WebResource webResource = wsClient.resource(BASE_URI);
        ClientResponse clientResponse = webResource.path("uniqueIds").path(guid).delete(ClientResponse.class);

        printClientResponseInfo(clientResponse);

        if(clientResponse.getStatus() == Status.NO_CONTENT.getStatusCode()) {
            System.out.println("UniqueId successfully deleted. guid = " + guid);
        } else {
            ErrorStub errorStub = clientResponse.getEntity(ErrorStub.class);
            System.out.println("Error = " + errorStub);
            //String message = clientResponse.getEntity(String.class);
            //System.out.println("Error message = " + message);
        }
    }

}
