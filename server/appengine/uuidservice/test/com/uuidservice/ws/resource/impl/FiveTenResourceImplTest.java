package com.uuidservice.ws.resource.impl;

import static org.junit.Assert.fail;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Iterator;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.uuidservice.ws.FiveTen;
import com.uuidservice.ws.stub.ErrorStub;
import com.uuidservice.ws.stub.FiveTenStub;
import com.uuidservice.ws.stub.FiveTenListStub;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.ClientResponse.Status;
import com.sun.jersey.api.client.WebResource;

public class FiveTenResourceImplTest
{
    private final static String BASE_URI = "http://localhost:8888/v1";

    // WS client.
    private Client wsClient = null;
    
    private void printClientResponseInfo(ClientResponse clientResponse)
    {
        int status = clientResponse.getStatus();
        System.out.println("Status = " + status);
        MultivaluedMap<String,String> headers = clientResponse.getHeaders();
        Iterator<MultivaluedMap.Entry<String,List<String>>> it = headers.entrySet().iterator();
        while(it.hasNext()) {
            MultivaluedMap.Entry<String,List<String>> m =(MultivaluedMap.Entry<String,List<String>>) it.next();
            String k = (String) m.getKey();
            List<String> l = (m.getValue() == null) ? new ArrayList<String>() : m.getValue(); // ???
            System.out.println("Header key = " + k);
            for(String v : l) {
                System.out.println("       value = " + v);                    
            }
        }
    }

    @Before
    public void setUp() throws Exception
    {
        //ClientConfig config = new DefaultClientConfig();
        //config.getClasses().add(JSONRootElementProvider.class);
        //wsClient = Client.create(config);
        //wsClient.addFilter(new LoggingFilter());

        wsClient = Client.create();
    }

    @After
    public void tearDown() throws Exception
    {
        //wsClient.destroy();
    }

    @Test
    public void testGetAllFiveTens()
    {
        WebResource webResource = wsClient.resource(BASE_URI);
        ClientResponse clientResponse = webResource.path("fiveTens").path("all").accept(MediaType.APPLICATION_XML).get(ClientResponse.class);
        
        printClientResponseInfo(clientResponse);

        if(clientResponse.getStatus() == Status.OK.getStatusCode()) {
            FiveTenListStub stub = clientResponse.getEntity(FiveTenListStub.class);
            System.out.println("FiveTen list stub = " + stub);
        } else {
            ErrorStub errorStub = clientResponse.getEntity(ErrorStub.class);
            System.out.println("Error = " + errorStub);
        }
    }

    @Test
    public void testFindFiveTens()
    {
        fail("Not yet implemented");
    }

    @Test
    public void testGetFiveTen()
    {
        //String guid = "fake_guid";
        //String guid = "123abc";
        String guid = "11e6ba6c-a003-40f4-851e-03035d9dbac4";
        WebResource webResource = wsClient.resource(BASE_URI);
        //ClientResponse clientResponse = webResource.path("fiveTens").path(guid).accept(MediaType.TEXT_PLAIN).get(ClientResponse.class);
        ClientResponse clientResponse = webResource.path("fiveTens").path(guid).accept(MediaType.APPLICATION_XML).get(ClientResponse.class);
        //ClientResponse clientResponse = webResource.path("fiveTens").path(guid).accept(MediaType.APPLICATION_JSON).get(ClientResponse.class);

        printClientResponseInfo(clientResponse);

        if(clientResponse.getStatus() == Status.OK.getStatusCode()) {
            FiveTenStub stub = clientResponse.getEntity(FiveTenStub.class);
            System.out.println("FiveTen stub = " + stub);
        } else {
            ErrorStub errorStub = clientResponse.getEntity(ErrorStub.class);
            System.out.println("Error = " + errorStub);
            //String message = clientResponse.getEntity(String.class);
            //System.out.println("Error message = " + message);
        }
    }

    @Test
    public void testGetFiveTenWithField()
    {
        //String guid = "fake_guid";
        //String guid = "123abc";
        String guid = "11e6ba6c-a003-40f4-851e-03035d9dbac4";
        //String field = "name";
        String field = "createdTime";
        WebResource webResource = wsClient.resource(BASE_URI);
        ClientResponse clientResponse = webResource.path("fiveTens").path(guid).path(field).accept(MediaType.TEXT_PLAIN).get(ClientResponse.class);
        //ClientResponse clientResponse = webResource.path("fiveTens").path(guid).path(field).accept(MediaType.APPLICATION_XML).get(ClientResponse.class);
        //ClientResponse clientResponse = webResource.path("fiveTens").path(guid).path(field).accept(MediaType.APPLICATION_JSON).get(ClientResponse.class);

        printClientResponseInfo(clientResponse);

        if(clientResponse.getStatus() == Status.OK.getStatusCode()) {
            String obj = clientResponse.getEntity(String.class);
            System.out.println("Object = " + obj);
        } else {
            String errorMsg = clientResponse.getEntity(String.class);
            System.out.println("ErrorMsg = " + errorMsg);
        }
    }

    @Test
    public void testCreateFiveTenStub()
    {
        FiveTen bean = new FiveTen() {
            private String guid;
            private Integer counter;
            private String requesterIpAddress;
            private Long createdTime;
            private Long modifiedTime;

            @Override
            public String getGuid()
            {
                return guid;
            }

            @Override
            public Integer getCounter()
            {
                return counter;
            }

            @Override
            public String getRequesterIpAddress()
            {
                return requesterIpAddress;
            }

            @Override
            public Long getCreatedTime()
            {
                return createdTime;
            }

            @Override
            public Long getModifiedTime()
            {
                return modifiedTime;
            }

        };
        
        
        FiveTenStub stub = new FiveTenStub(bean);
        //FiveTenStub stub = new FiveTenStub();
        //FiveTenStub stub = null;
        
        WebResource webResource = wsClient.resource(BASE_URI);
        //ClientResponse clientResponse = webResource.path("fiveTens").entity(stub, MediaType.APPLICATION_XML).post(ClientResponse.class);
        ClientResponse clientResponse = webResource.path("fiveTens").accept(MediaType.APPLICATION_XML).entity(stub, MediaType.APPLICATION_XML).post(ClientResponse.class);

        printClientResponseInfo(clientResponse);

        if(clientResponse.getStatus() == Status.CREATED.getStatusCode()) {
            String guid = clientResponse.getEntity(String.class);
            System.out.println("New fiveTen guid = " + guid);
        } else {
            ErrorStub errorStub = clientResponse.getEntity(ErrorStub.class);
            System.out.println("Error = " + errorStub);
            //String message = clientResponse.getEntity(String.class);
            //System.out.println("Error message = " + message);
        }
    }

    @Test
    public void testUpdateFiveTenStringFiveTenStub()
    {
        final String fiveTenGuid = "4e87c81523559c184a280628d82704d860f31474";
        
        FiveTen bean = new FiveTen() {
            private String guid;
            private Integer counter;
            private String requesterIpAddress;
            private Long createdTime;
            private Long modifiedTime;

            @Override
            public String getGuid()
            {
                return guid;
            }

            @Override
            public Integer getCounter()
            {
                return counter;
            }

            @Override
            public String getRequesterIpAddress()
            {
                return requesterIpAddress;
            }

            @Override
            public Long getCreatedTime()
            {
                return createdTime;
            }

            @Override
            public Long getModifiedTime()
            {
                return modifiedTime;
            }

        };
        
        FiveTenStub stub = new FiveTenStub(bean);
        //FiveTenStub stub = new FiveTenStub();
        //FiveTenStub stub = null;
        
        WebResource webResource = wsClient.resource(BASE_URI);
        ClientResponse clientResponse = webResource.path("fiveTens").path(fiveTenGuid).entity(stub, MediaType.APPLICATION_XML).put(ClientResponse.class);

        printClientResponseInfo(clientResponse);

        if(clientResponse.getStatus() == Status.NO_CONTENT.getStatusCode()) {
            System.out.println("FiveTen successfully updated.");
        } else {
            ErrorStub errorStub = clientResponse.getEntity(ErrorStub.class);
            System.out.println("Error = " + errorStub);
            //String message = clientResponse.getEntity(String.class);
            //System.out.println("Error message = " + message);
        }
    }

    @Test
    public void testDeleteFiveTen()
    {
        //String guid = "123abc";
        String guid = "d87a09c94b1c701ee98a7b6ade062c50c2d26d82";
        
        WebResource webResource = wsClient.resource(BASE_URI);
        ClientResponse clientResponse = webResource.path("fiveTens").path(guid).delete(ClientResponse.class);

        printClientResponseInfo(clientResponse);

        if(clientResponse.getStatus() == Status.NO_CONTENT.getStatusCode()) {
            System.out.println("FiveTen successfully deleted. guid = " + guid);
        } else {
            ErrorStub errorStub = clientResponse.getEntity(ErrorStub.class);
            System.out.println("Error = " + errorStub);
            //String message = clientResponse.getEntity(String.class);
            //System.out.println("Error message = " + message);
        }
    }

}
