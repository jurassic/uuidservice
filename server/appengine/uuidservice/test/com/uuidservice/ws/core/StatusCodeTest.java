package com.uuidservice.ws.core;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class StatusCodeTest
{

    @Before
    public void setUp() throws Exception
    {
    }

    @After
    public void tearDown() throws Exception
    {
    }

    @Test
    public void testIsInformational()
    {
        int code1 = StatusCode.CONTINUE;
        assertTrue(StatusCode.isInformational(code1));
        int code2 = StatusCode.OK;
        assertFalse(StatusCode.isInformational(code2));
    }

    @Test
    public void testIsSuccessful()
    {
        int code1 = StatusCode.OK;
        assertTrue(StatusCode.isSuccessful(code1));
        int code2 = StatusCode.BAD_REQUEST;
        assertFalse(StatusCode.isSuccessful(code2));
    }

    @Test
    public void testIsRedirection()
    {
        int code1 = StatusCode.FOUND;
        assertTrue(StatusCode.isRedirection(code1));
        int code2 = StatusCode.GONE;
        assertFalse(StatusCode.isRedirection(code2));
    }

    @Test
    public void testIsClientError()
    {
        int code1 = StatusCode.NOT_FOUND;
        assertTrue(StatusCode.isClientError(code1));
        int code2 = StatusCode.ACCEPTED;
        assertFalse(StatusCode.isClientError(code2));
    }

    @Test
    public void testIsServerError()
    {
        int code1 = StatusCode.SERVICE_UNAVAILABLE;
        assertTrue(StatusCode.isServerError(code1));
        int code2 = StatusCode.METHOD_NOT_ALLOWED;
        assertFalse(StatusCode.isServerError(code2));
    }

    @Test
    public void testIsError()
    {
        int code1 = StatusCode.INTERNAL_SERVER_ERROR;
        assertTrue(StatusCode.isError(code1));
        int code2 = StatusCode.UNSUPPORTED_MEDIA_TYPE;
        assertTrue(StatusCode.isError(code2));
        int code3 = StatusCode.CREATED;
        assertFalse(StatusCode.isError(code3));
    }

}
