package com.uuidservice.ws.core;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class GUIDTest
{

    @Before
    public void setUp() throws Exception
    {
    }

    @After
    public void tearDown() throws Exception
    {
    }

    @Test
    public void testGUID()
    {
        int count = 3;
        for(int i = 0; i<count; i++) {
            GUID guid = new GUID();
            System.out.println("guid = " + guid);
        }
    }

    @Test
    public void testHashCode()
    {
        int count = 3;
        for(int i = 0; i<count; i++) {
            int hashCode = new GUID().hashCode();
            System.out.println("guid hashCode = " + hashCode);
        }
    }

    @Test
    public void testFromString()
    {
        String s1 = "b2253d31-baa3-4a84-94cf-66f3591c4009";
        System.out.println("s1 = " + s1);
        GUID guid = GUID.fromString(s1);
        System.out.println("guid = " + guid);
        String s2 = guid.toString();
        System.out.println("s2 = " + s2);
        assertEquals(s1, s2);
    }

    @Test
    public void testEqualsObject()
    {
        GUID guid1 = new GUID();
        String s1 = guid1.toString();
        assertTrue(guid1.equals(s1));
        GUID guid2 = GUID.fromString(s1);
        assertTrue(guid1.equals(guid2));
        String s2 = guid2.toString();
        assertTrue(guid1.equals(s2));
        
        String s3 = "b2253d31-baa3-4a84-94cf-66f3591c4009";
        assertFalse(guid1.equals(s3));
        GUID guid3 = GUID.fromString(s3);
        assertFalse(guid1.equals(guid3));

        assertFalse(guid1.equals(100));
    }

    @Test
    public void testShortString()
    {
        int count = 10;
        for(int i=0; i<count; i++) {
            GUID guid1 = new GUID();
            String s1 = guid1.toShortString();
            System.out.println("guid1 = " + guid1 + "; s1 = " + s1);
            GUID guid2 = GUID.fromShortString(s1);
            System.out.println("guid2 = " + guid2);
            assertEquals(guid1, guid2);
        }
    }

}
