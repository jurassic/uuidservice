package com.uuidservice.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.uuidservice.ws.UniqueId;
import com.uuidservice.ws.util.CommonUtil;
import com.uuidservice.ws.core.GUID;

@PersistenceCapable(detachable="true")
public class UniqueIdDataObject extends KeyedDataObject implements UniqueId{
    private static final Logger log = Logger.getLogger(UniqueIdDataObject.class.getName());

    public static Key composeKey(String guid)
    {
        Key key = KeyFactory.createKey(UniqueIdDataObject.class.getSimpleName(), guid);
        return key; 
    }

    @Persistent(defaultFetchGroup = "true")
    private String guid;

    @Persistent(defaultFetchGroup = "true")
    private String name;

    @Persistent(defaultFetchGroup = "true")
    private String requesterUserAgent;

    @Persistent(defaultFetchGroup = "true")
    private String requesterHostname;

    @Persistent(defaultFetchGroup = "true")
    private String requesterIpAddress;

    public UniqueIdDataObject()
    {
        this(null);
    }
    public UniqueIdDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null);
    }
    public UniqueIdDataObject(String guid, String name, String requesterUserAgent, String requesterHostname, String requesterIpAddress)
    {
        this(guid, name, requesterUserAgent, requesterHostname, requesterIpAddress, null, null);
    }
    public UniqueIdDataObject(String guid, String name, String requesterUserAgent, String requesterHostname, String requesterIpAddress, Long createdTime, Long modifiedTime)
    {
        super(createdTime, modifiedTime);
        generatePK(guid);

        this.name = name;
        this.requesterUserAgent = requesterUserAgent;
        this.requesterHostname = requesterHostname;
        this.requesterIpAddress = requesterIpAddress;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        generatePK(guid);
    }
    protected void generatePK(String guid)
    {
        if(guid == null) {
            this.guid = GUID.generate();  // temporary
            log.fine("Guid has been set to a new guid = " + this.guid);
        } else {
            this.guid = guid;
            log.fine("Guid has been set to guid = " + this.guid);
        }
        rebuildKey();        
    }

    @Override
    protected Key createKey()
    {
        return UniqueIdDataObject.composeKey(getGuid());
    }

    public String getName()
    {
        return this.name;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public String getRequesterUserAgent()
    {
        return this.requesterUserAgent;
    }
    public void setRequesterUserAgent(String requesterUserAgent)
    {
        this.requesterUserAgent = requesterUserAgent;
    }

    public String getRequesterHostname()
    {
        return this.requesterHostname;
    }
    public void setRequesterHostname(String requesterHostname)
    {
        this.requesterHostname = requesterHostname;
    }

    public String getRequesterIpAddress()
    {
        return this.requesterIpAddress;
    }
    public void setRequesterIpAddress(String requesterIpAddress)
    {
        this.requesterIpAddress = requesterIpAddress;
    }

    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("guid", this.guid);
        dataMap.put("name", this.name);
        dataMap.put("requesterUserAgent", this.requesterUserAgent);
        dataMap.put("requesterHostname", this.requesterHostname);
        dataMap.put("requesterIpAddress", this.requesterIpAddress);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        UniqueId thatObj = (UniqueId) obj;
        if( (this.guid == null && thatObj.getGuid() != null)
            || (this.guid != null && thatObj.getGuid() == null)
            || !this.guid.equals(thatObj.getGuid()) ) {
            return false;
        }
        if( (this.name == null && thatObj.getName() != null)
            || (this.name != null && thatObj.getName() == null)
            || !this.name.equals(thatObj.getName()) ) {
            return false;
        }
        if( (this.requesterUserAgent == null && thatObj.getRequesterUserAgent() != null)
            || (this.requesterUserAgent != null && thatObj.getRequesterUserAgent() == null)
            || !this.requesterUserAgent.equals(thatObj.getRequesterUserAgent()) ) {
            return false;
        }
        if( (this.requesterHostname == null && thatObj.getRequesterHostname() != null)
            || (this.requesterHostname != null && thatObj.getRequesterHostname() == null)
            || !this.requesterHostname.equals(thatObj.getRequesterHostname()) ) {
            return false;
        }
        if( (this.requesterIpAddress == null && thatObj.getRequesterIpAddress() != null)
            || (this.requesterIpAddress != null && thatObj.getRequesterIpAddress() == null)
            || !this.requesterIpAddress.equals(thatObj.getRequesterIpAddress()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        hash = 31 * hash + delta;
        delta = name == null ? 0 : name.hashCode();
        hash = 31 * hash + delta;
        delta = requesterUserAgent == null ? 0 : requesterUserAgent.hashCode();
        hash = 31 * hash + delta;
        delta = requesterHostname == null ? 0 : requesterHostname.hashCode();
        hash = 31 * hash + delta;
        delta = requesterIpAddress == null ? 0 : requesterIpAddress.hashCode();
        hash = 31 * hash + delta;
        return hash;
    }

}
