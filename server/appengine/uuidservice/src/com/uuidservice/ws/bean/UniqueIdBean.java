package com.uuidservice.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.uuidservice.ws.UniqueId;
import com.uuidservice.ws.data.UniqueIdDataObject;

public class UniqueIdBean extends BeanBase implements UniqueId
{
    private static final Logger log = Logger.getLogger(UniqueIdBean.class.getName());

    // Embedded data object.
    private UniqueIdDataObject dobj = null;

    public UniqueIdBean()
    {
        this(new UniqueIdDataObject());
    }
    public UniqueIdBean(String guid)
    {
        this(new UniqueIdDataObject(guid));
    }
    public UniqueIdBean(UniqueIdDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public UniqueIdDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getGuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getGuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UniqueIdDataObject is null!");
            return null;
        }
    }
    public void setGuid(String guid)
    {
        if(getDataObject() != null) {
            // Unfortunately, due to the way Stub object is instantiated (e.g., from web service request)
            // we should allow setting guid after the object has been constructed.
            log.info("Bean.guid is being set to a new guid = " + guid);
            getDataObject().setGuid(guid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UniqueIdDataObject is null!");
        }
    }

    public String getName()
    {
        if(getDataObject() != null) {
            return getDataObject().getName();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UniqueIdDataObject is null!");
            return null;   // ???
        }
    }
    public void setName(String name)
    {
        if(getDataObject() != null) {
            getDataObject().setName(name);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UniqueIdDataObject is null!");
        }
    }

    public String getRequesterUserAgent()
    {
        if(getDataObject() != null) {
            return getDataObject().getRequesterUserAgent();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UniqueIdDataObject is null!");
            return null;   // ???
        }
    }
    public void setRequesterUserAgent(String requesterUserAgent)
    {
        if(getDataObject() != null) {
            getDataObject().setRequesterUserAgent(requesterUserAgent);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UniqueIdDataObject is null!");
        }
    }

    public String getRequesterHostname()
    {
        if(getDataObject() != null) {
            return getDataObject().getRequesterHostname();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UniqueIdDataObject is null!");
            return null;   // ???
        }
    }
    public void setRequesterHostname(String requesterHostname)
    {
        if(getDataObject() != null) {
            getDataObject().setRequesterHostname(requesterHostname);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UniqueIdDataObject is null!");
        }
    }

    public String getRequesterIpAddress()
    {
        if(getDataObject() != null) {
            return getDataObject().getRequesterIpAddress();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UniqueIdDataObject is null!");
            return null;   // ???
        }
    }
    public void setRequesterIpAddress(String requesterIpAddress)
    {
        if(getDataObject() != null) {
            getDataObject().setRequesterIpAddress(requesterIpAddress);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UniqueIdDataObject is null!");
        }
    }

    // TBD
    public UniqueIdDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
