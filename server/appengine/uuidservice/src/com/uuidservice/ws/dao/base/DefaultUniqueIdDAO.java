package com.uuidservice.ws.dao.base;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;
import com.google.appengine.api.datastore.Key;

import com.uuidservice.ws.BaseException;
import com.uuidservice.ws.exception.DataStoreException;
import com.uuidservice.ws.exception.ServiceUnavailableException;
import com.uuidservice.ws.exception.RequestConflictException;
import com.uuidservice.ws.exception.ResourceNotFoundException;
import com.uuidservice.ws.exception.NotImplementedException;
import com.uuidservice.ws.dao.UniqueIdDAO;
import com.uuidservice.ws.data.UniqueIdDataObject;


public class DefaultUniqueIdDAO extends DefaultDAOBase implements UniqueIdDAO
{
    private static final Logger log = Logger.getLogger(DefaultUniqueIdDAO.class.getName()); 

    // Returns the uniqueId for the given guid.
    // Returns null if none is found in the datastore.
    @Override
    public UniqueIdDataObject getUniqueId(String guid) throws BaseException
    {
        UniqueIdDataObject uniqueId = null;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            //Transaction tx = pm.currentTransaction();
            try {
                //tx.begin();
                Key key = UniqueIdDataObject.composeKey(guid);
                uniqueId = pm.getObjectById(UniqueIdDataObject.class, key);
                //tx.commit();
            } catch(Exception ex) {
                log.log(Level.WARNING, "Failed to retrieve uniqueId for guid = " + guid, ex);
                throw new ResourceNotFoundException(ex);
            } finally {
                try {
                    //if(tx.isActive()) {
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.WARNING, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }
	    return uniqueId;
	}

    @Override
    public List<UniqueIdDataObject> getAllUniqueIds() throws BaseException
	{
	    return getAllUniqueIds(null, null, null);
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<UniqueIdDataObject> getAllUniqueIds(String ordering, Long offset, Integer count) throws BaseException
	{
    	List<UniqueIdDataObject> uniqueIds = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.begin();
            Query q = pm.newQuery(UniqueIdDataObject.class);
            if(ordering != null) {
                q.setOrdering(ordering);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            uniqueIds = (List<UniqueIdDataObject>) q.execute();
            uniqueIds.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            /*
            // ???
            Collection<UniqueIdDataObject> rs_uniqueIds = (Collection<UniqueIdDataObject>) q.execute();
            if(rs_uniqueIds == null) {
                log.log(Level.WARNING, "Failed to retrieve all uniqueIds.");
                uniqueIds = new ArrayList<UniqueIdDataObject>();  // ???           
            } else {
                uniqueIds = new ArrayList<UniqueIdDataObject>(pm.detachCopyAll(rs_uniqueIds));
            }
            */
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all uniqueIds.", ex);
            //uniqueIds = new ArrayList<UniqueIdDataObject>();  // ???
            throw new DataStoreException("Failed to retrieve all uniqueIds.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.WARNING, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }
        return uniqueIds;
    }

    @Override
	public List<UniqueIdDataObject> findUniqueIds(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findUniqueIds(filter, ordering, params, values, null, null);
    }

    @Override
	public List<UniqueIdDataObject> findUniqueIds(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findUniqueIds(filter, ordering, params, values, null, null, offset, count);
	}

    @SuppressWarnings("unchecked")
    @Override
	public List<UniqueIdDataObject> findUniqueIds(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
        log.info("DefaultUniqueIdDAO.findUniqueIds(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("findUniqueIds() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        List<UniqueIdDataObject> uniqueIds = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.begin();
            Query q = pm.newQuery(UniqueIdDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }
            if(offset == null && count == null) {
                // ignore range.
            } else {
                long fromIncl;
                long toExcl;
                if(offset == null || offset < 0L) {
                    fromIncl = 0L;
                } else {
                    fromIncl = offset;
                }
                if(count == null || count < 0) {
                    toExcl = Long.MAX_VALUE;
                } else {
                    toExcl = fromIncl + count;
                }
                q.setRange(fromIncl, toExcl);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                uniqueIds = (List<UniqueIdDataObject>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                uniqueIds = (List<UniqueIdDataObject>) q.execute();
            }
            uniqueIds.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.
            //while($TypeUtil.deCapitalizePluralize($typeName).iterator().hasNext()) {
            //    for(UniqueIdDataObject dobj : uniqueIds) {
            //        //
            //    }
            //}
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find uniqueIds because index is missing.", ex);
            //uniqueIds = new ArrayList<UniqueIdDataObject>();  // ???
            throw new DataStoreException("Failed to find uniqueIds because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find uniqueIds meeting the criterion.", ex);
            //uniqueIds = new ArrayList<UniqueIdDataObject>();  // ???
            throw new DataStoreException("Failed to find uniqueIds meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.WARNING, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }
        return uniqueIds;
	}

    @SuppressWarnings("unchecked")
    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        log.info("DefaultUniqueIdDAO.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("getCount() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        Long count = 0L; 
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.begin();
            Query q = pm.newQuery(UniqueIdDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(aggregate == null || aggregate.isEmpty()) {
                aggregate = "count(this)";  // By default, count. Other examples: "max(this.createdTime)", avg, sum, etc. (Note: These are currently not supported by Google App Engine.)
            }
            q.setResult(aggregate);
            //q.setResultClass(Long.class);
            // For now, we only support a single aggregate function. (Eg, no comma-separated list.)
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = new Long((Integer) q.executeWithArray(values.toArray(new Object[0])));
            } else {
                count = new Long((Integer) q.execute());
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to get uniqueId count because index is missing.", ex);
            throw new DataStoreException("Failed to get uniqueId count because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to get uniqueId count meeting the criterion.", ex);
            throw new DataStoreException("Failed to get uniqueId count meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.WARNING, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }
        return count;
    }

	// Stores the uniqueId in the datastore and returns its guid.
	// Returns null if the operation fails.
    private String storeUniqueId(UniqueIdDataObject uniqueId) throws BaseException
    {
        String guid = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
	    try {
            //tx.begin();
            // TBD: If the object on the datastore has non-null created date and the arg shortLink has no created date timestamp,
            //      do not overwrite the timestamp. How to do that???
            // For now, this is ok since we use "overwrite/replace" semantics (not partial update).
            Long createdTime = uniqueId.getCreatedTime();
            if(createdTime == null) {
                createdTime = (new Date()).getTime();
                uniqueId.setCreatedTime(createdTime);
            }
            Long modifiedTime = uniqueId.getModifiedTime();
            if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
                uniqueId.setModifiedTime(createdTime);
            }
            pm.makePersistent(uniqueId); 
            //tx.commit();
            // TBD: How do you know the makePersistent() call was successful???
            guid = uniqueId.getGuid();
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to store uniqueId because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to store uniqueId because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to store uniqueId.", ex);
            throw new DataStoreException("Failed to store uniqueId.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.WARNING, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }
        return guid;
    }
	
    @Override
    public String createUniqueId(UniqueIdDataObject uniqueId) throws BaseException
    {
        // The createdTime field will be automatically set in storeUniqueId().
        //Long createdTime = uniqueId.getCreatedTime();
        //if(createdTime == null) {
        //    createdTime = (new Date()).getTime();
        //    uniqueId.setCreatedTime(createdTime);
        //}
        //Long modifiedTime = uniqueId.getModifiedTime();
        //if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
        //    uniqueId.setModifiedTime(createdTime);
        //}
        return storeUniqueId(uniqueId);
    }

    @Override
	public Boolean updateUniqueId(UniqueIdDataObject uniqueId) throws BaseException
	{
	    // Modify the modifiedTime timestamp.
	    // CreatedTime should, normally, be non-null.
	    // If null, however, it will be automatically set in storeUniqueId()
	    // (in which case modifiedTime might be updated again).
	    uniqueId.setModifiedTime((new Date()).getTime());
	    String guid = storeUniqueId(uniqueId);
	    if(guid != null) {
	        return true;
	    } else {
	        return false;
	    }
	}
	
    @Override
    public Boolean deleteUniqueId(UniqueIdDataObject uniqueId) throws BaseException
    {
        boolean suc = false;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.begin();
            pm.deletePersistent(uniqueId);
            //tx.commit();
            // TBD: How do you know the deletePersistent() call was successful???
            suc = true;
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to delete uniqueId because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete uniqueId because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete uniqueId.", ex);
            throw new DataStoreException("Failed to delete uniqueId.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.WARNING, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }
        return suc;
    }

    @Override
    public Boolean deleteUniqueId(String guid) throws BaseException
    {
        boolean suc = false;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            //Transaction tx = pm.currentTransaction();
            try {
                //tx.begin();
                Key key = UniqueIdDataObject.composeKey(guid);
                UniqueIdDataObject uniqueId = pm.getObjectById(UniqueIdDataObject.class, key);
                pm.deletePersistent(uniqueId);
                //tx.commit();
                // TBD: How do you know the deletePersistent() call was successful???
                suc = true;
            } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
                // Datastore is read-only, possibly due to the Google App Engine maintenance.
                log.log(Level.WARNING, "Failed to delete uniqueId because the datastore is currently read-only.", ex);
                throw new ServiceUnavailableException("Failed to delete uniqueId because the datastore is currently read-only.", ex);
            } catch(Exception ex) {
                log.log(Level.WARNING, "Failed to delete uniqueId for guid = " + guid, ex);
                throw new DataStoreException("Failed to delete uniqueId for guid = " + guid, ex);
            } finally {
                try {
                    //if(tx.isActive()) {
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.WARNING, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }
        return suc;
	}

    @Override
    public Long deleteUniqueIds(String filter, String params, List<String> values) throws BaseException
    {
        log.info("DefaultUniqueIdDAO.deleteUniqueIds(): filter=" + filter + "; params=" + params);

        // TBD: Delete-by-query requests should be carefully checked because of its potential harms.
        if(filter == null || filter.isEmpty()) {
            throw new RequestConflictException("deleteAll() without filter is not supported for performance/safety reasons");
        }

        Long count = 0L;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.begin();
            Query q = pm.newQuery(UniqueIdDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = (Long) q.deletePersistentAll(values.toArray(new Object[0]));
            } else {
                count = (Long) q.deletePersistentAll();
            }
            //tx.commit();
            // TBD: How do you know the deletePersistent() call was successful???
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to deleteuniqueIds because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete uniqueIds because the datastore is currently read-only.", ex);
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to delete uniqueIds because index is missing", ex);
            throw new DataStoreException("Failed to delete uniqueIds because index is missing", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete uniqueIds", ex);
            throw new DataStoreException("Failed to delete uniqueIds", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.WARNING, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        return count;
    }

}
