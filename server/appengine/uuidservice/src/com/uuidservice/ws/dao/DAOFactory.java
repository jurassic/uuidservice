package com.uuidservice.ws.dao;

// Abstract factory.
public abstract class DAOFactory
{
    public abstract UniqueIdDAO getUniqueIdDAO();
    public abstract FiveTenDAO getFiveTenDAO();
}
