package com.uuidservice.ws.dao.base;

import java.util.logging.Logger;

import com.uuidservice.ws.dao.DAOFactory;
import com.uuidservice.ws.dao.UniqueIdDAO;
import com.uuidservice.ws.dao.FiveTenDAO;

// Default DAO factory uses JDO on Google AppEngine.
public class DefaultDAOFactory extends DAOFactory
{
    private static final Logger log = Logger.getLogger(DefaultDAOFactory.class.getName());

    // Ctor. Prevents instantiation.
    private DefaultDAOFactory()
    {
        // ...
    }

    // Initialization-on-demand holder.
    private static class DefaultDAOFactoryHolder
    {
        private static final DefaultDAOFactory INSTANCE = new DefaultDAOFactory();
    }

    // Singleton method
    public static DefaultDAOFactory getInstance()
    {
        return DefaultDAOFactoryHolder.INSTANCE;
    }

    @Override
    public UniqueIdDAO getUniqueIdDAO()
    {
        return new DefaultUniqueIdDAO();
    }

    @Override
    public FiveTenDAO getFiveTenDAO()
    {
        return new DefaultFiveTenDAO();
    }

}
