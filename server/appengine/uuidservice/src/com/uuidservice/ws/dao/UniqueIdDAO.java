package com.uuidservice.ws.dao;

import java.util.List;

import com.uuidservice.ws.BaseException;
import com.uuidservice.ws.data.UniqueIdDataObject;

// TBD: Add offset/count to getAllUniqueIds() and findUniqueIds(), etc.
public interface UniqueIdDAO
{
    UniqueIdDataObject getUniqueId(String guid) throws BaseException;
    List<UniqueIdDataObject> getAllUniqueIds() throws BaseException;
    List<UniqueIdDataObject> getAllUniqueIds(String ordering, Long offset, Integer count) throws BaseException;
    List<UniqueIdDataObject> findUniqueIds(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<UniqueIdDataObject> findUniqueIds(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    List<UniqueIdDataObject> findUniqueIds(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createUniqueId(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return UniqueIdDataObject?)
    String createUniqueId(UniqueIdDataObject uniqueId) throws BaseException;          // Returns Guid.  (Return UniqueIdDataObject?)
    //Boolean updateUniqueId(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateUniqueId(UniqueIdDataObject uniqueId) throws BaseException;
    Boolean deleteUniqueId(String guid) throws BaseException;
    Boolean deleteUniqueId(UniqueIdDataObject uniqueId) throws BaseException;
    Long deleteUniqueIds(String filter, String params, List<String> values) throws BaseException;
}
