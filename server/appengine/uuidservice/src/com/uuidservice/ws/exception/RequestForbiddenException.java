package com.uuidservice.ws.exception;

import com.uuidservice.ws.BaseException;


public class RequestForbiddenException extends BaseException 
{
    private static final long serialVersionUID = 1L;

    public RequestForbiddenException() 
    {
        super();
    }
    public RequestForbiddenException(String message) 
    {
        super(message);
    }
    public RequestForbiddenException(String message, Throwable cause) 
    {
        super(message, cause);
    }
    public RequestForbiddenException(Throwable cause) 
    {
        super(cause);
    }

}
