package com.uuidservice.ws.util;

import java.util.logging.Logger;
import java.util.logging.Level;
import org.codehaus.jackson.map.ObjectMapper;

public class JsonUtil
{
    private static final Logger log = Logger.getLogger(JsonUtil.class.getName()); 
    private static ObjectMapper jsonObjectMapper = null;

    private JsonUtil() {}

    public static ObjectMapper getJsonObjectMapper()
    {
        if(jsonObjectMapper == null) {
            jsonObjectMapper = new ObjectMapper();
        }
        return jsonObjectMapper;
    }
    
}
