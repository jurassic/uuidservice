package com.uuidservice.ws.service;

import java.util.List;

import com.uuidservice.ws.UniqueId;
import com.uuidservice.ws.BaseException;

// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface UniqueIdService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    UniqueId getUniqueId(String guid) throws BaseException;
    Object getUniqueId(String guid, String field) throws BaseException;
    List<UniqueId> getAllUniqueIds() throws BaseException;
    List<UniqueId> getAllUniqueIds(String ordering, Long offset, Integer count) throws BaseException;
    List<UniqueId> findUniqueIds(String filter, String ordering, String params, List<String> values) throws BaseException;
    List<UniqueId> findUniqueIds(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createUniqueId(String name, String requesterUserAgent, String requesterHostname, String requesterIpAddress) throws BaseException;
    //String createUniqueId(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return UniqueId?)
    String createUniqueId(UniqueId uniqueId) throws BaseException;          // Returns Guid.  (Return UniqueId?)
    Boolean updateUniqueId(String guid, String name, String requesterUserAgent, String requesterHostname, String requesterIpAddress) throws BaseException;
    //Boolean updateUniqueId(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateUniqueId(UniqueId uniqueId) throws BaseException;
    Boolean deleteUniqueId(String guid) throws BaseException;
    Boolean deleteUniqueId(UniqueId uniqueId) throws BaseException;
    Long deleteUniqueIds(String filter, String params, List<String> values) throws BaseException;

}
