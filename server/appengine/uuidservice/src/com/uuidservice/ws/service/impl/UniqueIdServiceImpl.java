package com.uuidservice.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.uuidservice.ws.BaseException;
import com.uuidservice.ws.exception.BadRequestException;
import com.uuidservice.ws.UniqueId;
import com.uuidservice.ws.bean.UniqueIdBean;
import com.uuidservice.ws.dao.DAOFactory;
import com.uuidservice.ws.data.UniqueIdDataObject;
import com.uuidservice.ws.service.DAOFactoryManager;
import com.uuidservice.ws.service.UniqueIdService;

// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class UniqueIdServiceImpl implements UniqueIdService
{
    private static final Logger log = Logger.getLogger(UniqueIdServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // UniqueId related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public UniqueId getUniqueId(String guid) throws BaseException
    {
        UniqueIdDataObject dataObj = getDAOFactory().getUniqueIdDAO().getUniqueId(guid);
        if(dataObj == null) {
            log.log(Level.WARNING, "Failed to retrieve UniqueIdDataObject for guid = " + guid);
            return null;  // ????
        }
        UniqueIdBean bean = new UniqueIdBean(dataObj);
        return bean;
    }

    @Override
    public Object getUniqueId(String guid, String field) throws BaseException
    {
        UniqueIdDataObject dataObj = getDAOFactory().getUniqueIdDAO().getUniqueId(guid);
        if(dataObj == null) {
            log.log(Level.WARNING, "Failed to retrieve UniqueIdDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("name")) {
            return dataObj.getName();
        } else if(field.equals("requesterUserAgent")) {
            return dataObj.getRequesterUserAgent();
        } else if(field.equals("requesterHostname")) {
            return dataObj.getRequesterHostname();
        } else if(field.equals("requesterIpAddress")) {
            return dataObj.getRequesterIpAddress();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }
    
    @Override
    public List<UniqueId> getAllUniqueIds() throws BaseException
    {
        return getAllUniqueIds(null, null, null);
    }

    @Override
    public List<UniqueId> getAllUniqueIds(String ordering, Long offset, Integer count) throws BaseException
    {
        // TBD: Is there a better way????
        List<UniqueId> list = new ArrayList<UniqueId>();
        List<UniqueIdDataObject> dataObjs = getDAOFactory().getUniqueIdDAO().getAllUniqueIds(ordering, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve UniqueIdDataObject list.");
        } else {
            Iterator<UniqueIdDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                UniqueIdDataObject dataObj = (UniqueIdDataObject) it.next();
                list.add(new UniqueIdBean(dataObj));
            }
        }
        return list;
    }

    @Override
    public List<UniqueId> findUniqueIds(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findUniqueIds(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<UniqueId> findUniqueIds(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        log.fine("UniqueIdServiceImpl.findUniqueIds(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count);

        // TBD: Is there a better way????
        List<UniqueId> list = new ArrayList<UniqueId>();
        List<UniqueIdDataObject> dataObjs = getDAOFactory().getUniqueIdDAO().findUniqueIds(filter, ordering, params, values, grouping, unique, offset, count);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find uniqueIds for the given criterion.");
        } else {
            Iterator<UniqueIdDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                UniqueIdDataObject dataObj = (UniqueIdDataObject) it.next();
                list.add(new UniqueIdBean(dataObj));
            }
        }
        return list;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        log.fine("UniqueIdServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        Long count = getDAOFactory().getUniqueIdDAO().getCount(filter, params, values, aggregate);
        return count;
    }

    @Override
    public String createUniqueId(String name, String requesterUserAgent, String requesterHostname, String requesterIpAddress) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        UniqueIdDataObject dataObj = new UniqueIdDataObject(null, name, requesterUserAgent, requesterHostname, requesterIpAddress);
        return createUniqueId(dataObj);
    }

    @Override
    public String createUniqueId(UniqueId uniqueId) throws BaseException
    {
        // Param uniqueId cannot be null.....
        if(uniqueId == null) {
            log.log(Level.INFO, "Param uniqueId is null!");
            throw new BadRequestException("Param uniqueId object is null!");
        }
        UniqueIdDataObject dataObj = null;
        if(uniqueId instanceof UniqueIdDataObject) {
            dataObj = (UniqueIdDataObject) uniqueId;
        } else if(uniqueId instanceof UniqueIdBean) {
            dataObj = ((UniqueIdBean) uniqueId).toDataObject();
        } else {  // if(uniqueId instanceof UniqueId)
            //dataObj = new UniqueIdDataObject(null, uniqueId.getName(), uniqueId.getRequesterUserAgent(), uniqueId.getRequesterHostname(), uniqueId.getRequesterIpAddress());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new UniqueIdDataObject(uniqueId.getGuid(), uniqueId.getName(), uniqueId.getRequesterUserAgent(), uniqueId.getRequesterHostname(), uniqueId.getRequesterIpAddress());
        }
        String guid = getDAOFactory().getUniqueIdDAO().createUniqueId(dataObj);
        return guid;
    }

    @Override
    public Boolean updateUniqueId(String guid, String name, String requesterUserAgent, String requesterHostname, String requesterIpAddress) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        UniqueIdDataObject dataObj = new UniqueIdDataObject(guid, name, requesterUserAgent, requesterHostname, requesterIpAddress);
        return updateUniqueId(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateUniqueId(UniqueId uniqueId) throws BaseException
    {
        // Param uniqueId cannot be null.....
        if(uniqueId == null || uniqueId.getGuid() == null) {
            log.log(Level.INFO, "Param uniqueId or its guid is null!");
            throw new BadRequestException("Param uniqueId object or its guid is null!");
        }
        UniqueIdDataObject dataObj = null;
        if(uniqueId instanceof UniqueIdDataObject) {
            dataObj = (UniqueIdDataObject) uniqueId;
        } else if(uniqueId instanceof UniqueIdBean) {
            dataObj = ((UniqueIdBean) uniqueId).toDataObject();
        } else {  // if(uniqueId instanceof UniqueId)
            dataObj = new UniqueIdDataObject(uniqueId.getGuid(), uniqueId.getName(), uniqueId.getRequesterUserAgent(), uniqueId.getRequesterHostname(), uniqueId.getRequesterIpAddress());
        }
        Boolean suc = getDAOFactory().getUniqueIdDAO().updateUniqueId(dataObj);
        return suc;
    }
    
    @Override
    public Boolean deleteUniqueId(String guid) throws BaseException
    {
        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getUniqueIdDAO().deleteUniqueId(guid);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteUniqueId(UniqueId uniqueId) throws BaseException
    {
        // Param uniqueId cannot be null.....
        if(uniqueId == null || uniqueId.getGuid() == null) {
            log.log(Level.INFO, "Param uniqueId or its guid is null!");
            throw new BadRequestException("Param uniqueId object or its guid is null!");
        }
        UniqueIdDataObject dataObj = null;
        if(uniqueId instanceof UniqueIdDataObject) {
            dataObj = (UniqueIdDataObject) uniqueId;
        } else if(uniqueId instanceof UniqueIdBean) {
            dataObj = ((UniqueIdBean) uniqueId).toDataObject();
        } else {  // if(uniqueId instanceof UniqueId)
            dataObj = new UniqueIdDataObject(uniqueId.getGuid(), uniqueId.getName(), uniqueId.getRequesterUserAgent(), uniqueId.getRequesterHostname(), uniqueId.getRequesterIpAddress());
        }
        Boolean suc = getDAOFactory().getUniqueIdDAO().deleteUniqueId(dataObj);
        return suc;
    }

    @Override
    public Long deleteUniqueIds(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getDAOFactory().getUniqueIdDAO().deleteUniqueIds(filter, params, values);
        return count;
    }

}
