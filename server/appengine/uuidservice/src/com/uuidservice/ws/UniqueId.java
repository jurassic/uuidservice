package com.uuidservice.ws;


public interface UniqueId 
{
    String  getGuid();
    String  getName();
    String  getRequesterUserAgent();
    String  getRequesterHostname();
    String  getRequesterIpAddress();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
