package com.uuidservice.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

import com.uuidservice.ws.UniqueId;
import com.uuidservice.ws.util.JsonUtil;


@XmlRootElement(name = "uniqueIds")
public class UniqueIdListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UniqueIdListStub.class.getName());

    private List<UniqueIdStub> uniqueIds = null;

    public UniqueIdListStub()
    {
        this(new ArrayList<UniqueIdStub>());
    }
    public UniqueIdListStub(List<UniqueIdStub> uniqueIds)
    {
        this.uniqueIds = uniqueIds;
    }

    @XmlElement(name = "uniqueId")
    public List<UniqueIdStub> getList()
    {
        return uniqueIds;
    }
    public void setList(List<UniqueIdStub> uniqueIds)
    {
        this.uniqueIds = uniqueIds;
    }

    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Iterator<UniqueIdStub> it = this.uniqueIds.iterator();
        while(it.hasNext()) {
            UniqueIdStub uniqueId = it.next();
            sb.append(uniqueId.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static UniqueIdListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            log.log(Level.INFO, "Json string representation of UniqueIdListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write UniqueIdListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write UniqueIdListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write UniqueIdListStub object as a string.", e);
        }
        
        return null;
    }
    public static UniqueIdListStub fromJsonString(String jsonStr)
    {
        try {
            UniqueIdListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, UniqueIdListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into UniqueIdListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into UniqueIdListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into UniqueIdListStub object.", e);
        }
        
        return null;
    }

}
