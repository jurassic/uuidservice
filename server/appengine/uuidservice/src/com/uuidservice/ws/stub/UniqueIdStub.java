package com.uuidservice.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

import com.uuidservice.ws.UniqueId;
import com.uuidservice.ws.util.JsonUtil;


@XmlRootElement(name = "uniqueId")
@XmlType(propOrder = {"guid", "name", "requesterUserAgent", "requesterHostname", "requesterIpAddress", "createdTime", "modifiedTime"})
public class UniqueIdStub implements UniqueId, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UniqueIdStub.class.getName());

    private String guid;
    private String name;
    private String requesterUserAgent;
    private String requesterHostname;
    private String requesterIpAddress;
    private Long createdTime;
    private Long modifiedTime;

    public UniqueIdStub()
    {
        this(null);
    }
    public UniqueIdStub(UniqueId bean)
    {
        if(bean != null) {
            this.guid = bean.getGuid();
            this.name = bean.getName();
            this.requesterUserAgent = bean.getRequesterUserAgent();
            this.requesterHostname = bean.getRequesterHostname();
            this.requesterIpAddress = bean.getRequesterIpAddress();
            this.createdTime = bean.getCreatedTime();
            this.modifiedTime = bean.getModifiedTime();
        }
    }


    @XmlElement
    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    @XmlElement
    public String getName()
    {
        return this.name;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    @XmlElement
    public String getRequesterUserAgent()
    {
        return this.requesterUserAgent;
    }
    public void setRequesterUserAgent(String requesterUserAgent)
    {
        this.requesterUserAgent = requesterUserAgent;
    }

    @XmlElement
    public String getRequesterHostname()
    {
        return this.requesterHostname;
    }
    public void setRequesterHostname(String requesterHostname)
    {
        this.requesterHostname = requesterHostname;
    }

    @XmlElement
    public String getRequesterIpAddress()
    {
        return this.requesterIpAddress;
    }
    public void setRequesterIpAddress(String requesterIpAddress)
    {
        this.requesterIpAddress = requesterIpAddress;
    }

    @XmlElement
    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    @XmlElement
    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("guid", this.guid);
        dataMap.put("name", this.name);
        dataMap.put("requesterUserAgent", this.requesterUserAgent);
        dataMap.put("requesterHostname", this.requesterHostname);
        dataMap.put("requesterIpAddress", this.requesterIpAddress);
        dataMap.put("createdTime", this.createdTime);
        dataMap.put("modifiedTime", this.modifiedTime);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        hash = 31 * hash + delta;
        delta = name == null ? 0 : name.hashCode();
        hash = 31 * hash + delta;
        delta = requesterUserAgent == null ? 0 : requesterUserAgent.hashCode();
        hash = 31 * hash + delta;
        delta = requesterHostname == null ? 0 : requesterHostname.hashCode();
        hash = 31 * hash + delta;
        delta = requesterIpAddress == null ? 0 : requesterIpAddress.hashCode();
        hash = 31 * hash + delta;
        delta = createdTime == null ? 0 : createdTime.hashCode();
        hash = 31 * hash + delta;
        delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
        hash = 31 * hash + delta;
        return hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static UniqueIdStub convertBeanToStub(UniqueId bean)
    {
        UniqueIdStub stub = null;
        if(bean instanceof UniqueIdStub) {
            stub = (UniqueIdStub) bean;
        } else {
            if(bean != null) {
                stub = new UniqueIdStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static UniqueIdStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            log.log(Level.INFO, "Json string representation of UniqueIdStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write UniqueIdStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write UniqueIdStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write UniqueIdStub object as a string.", e);
        }
        
        return null;
    }
    public static UniqueIdStub fromJsonString(String jsonStr)
    {
        try {
            UniqueIdStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, UniqueIdStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into UniqueIdStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into UniqueIdStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into UniqueIdStub object.", e);
        }
        
        return null;
    }

}
