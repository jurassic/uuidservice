package com.uuidservice.ws.auth;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.sun.jersey.api.core.HttpRequestContext;
import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.oauth.server.OAuthServerRequest;
import com.sun.jersey.oauth.signature.OAuthParameters;
import com.sun.jersey.oauth.signature.OAuthSecrets;
import com.sun.jersey.oauth.signature.OAuthSignature;
import com.sun.jersey.oauth.signature.OAuthSignatureException;

import com.uuidservice.ws.BaseException;
import com.uuidservice.ws.exception.BadRequestException;
import com.uuidservice.ws.exception.UnauthorizedException;


public class TwoLeggedOAuthProvider
{
    private static final Logger log = Logger.getLogger(TwoLeggedOAuthProvider.class.getName());

    // Consumer key-secret map.
    private Map<String, String> consumerSecretMap;
    
    private TwoLeggedOAuthProvider()
    {
        // TBD: Populate this from data store ? 
        consumerSecretMap = new HashMap<String, String>();
        consumerSecretMap.put("ac567b5c-b8a0-4a54-92b4-78033dafff1b", "9db30167-5233-42e8-b277-e73695d3167c");
        // ...
    }

    private static class TwoLeggedOAuthProviderHolder {
        private static TwoLeggedOAuthProvider INSTANCE = new TwoLeggedOAuthProvider();
    };

    public static TwoLeggedOAuthProvider getInstance()
    {
        return TwoLeggedOAuthProviderHolder.INSTANCE;
    }
    
    public boolean verify(HttpRequestContext containerRequest) throws BaseException
    {
        OAuthServerRequest request = new OAuthServerRequest(containerRequest);

        OAuthParameters params = new OAuthParameters();
        params.readRequest(request);
       
        // Check that the timestamp has not expired
        String timestampStr = params.getTimestamp();
        // TBD: timestamp checking code ...
        String consumerKey = params.getConsumerKey();
        // TBD: get consumerSecret for the given consumerKey...
        String consumerSecret = consumerSecretMap.get(consumerKey);

        // Set the secret(s), against which we will verify the request
        OAuthSecrets secrets = new OAuthSecrets().consumerSecret(consumerSecret);

        // Verify the signature
        try {
            if(!OAuthSignature.verify(request, params, secrets)) {
                log.warning("Two-legged OAuth verification failed.");
                //throw new UnauthorizedException("Two-legged OAuth verification failed.");
                return false;
            }
        } catch (OAuthSignatureException e) {
            log.log(Level.WARNING, "OAuth signature error.", e);
            throw new BadRequestException("OAuth signature error.", e);  // ???
        } catch (Exception e) {
            log.log(Level.WARNING, "Two-legged OAuth verification failed due to unknown error.", e);
            throw new BaseException("Two-legged OAuth verification failed due to unknown error.", e);  // ???
        }

        return true;
    }
    
}
