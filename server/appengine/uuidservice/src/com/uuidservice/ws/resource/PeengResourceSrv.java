package com.uuidservice.ws.resource;

import java.util.Date;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.uuidservice.ws.BaseException;
import com.uuidservice.ws.CommonConstants;
import com.uuidservice.ws.core.GUID;
import com.uuidservice.ws.exception.BadRequestException;
import com.uuidservice.ws.exception.InternalServerErrorException;
import com.uuidservice.ws.exception.NotImplementedException;
import com.uuidservice.ws.exception.RequestConflictException;
import com.uuidservice.ws.exception.RequestForbiddenException;
import com.uuidservice.ws.exception.ServiceUnavailableException;
import com.uuidservice.ws.exception.resource.BaseResourceException;
import com.uuidservice.ws.resource.exception.BadRequestRsException;
import com.uuidservice.ws.resource.exception.InternalServerErrorRsException;
import com.uuidservice.ws.resource.exception.RequestConflictRsException;
import com.uuidservice.ws.resource.exception.RequestForbiddenRsException;
import com.uuidservice.ws.resource.exception.ServiceUnavailableRsException;


@Path("/peeng")
public class PeengResourceSrv
{
    private static final Logger log = Logger.getLogger(PeengResourceSrv.class.getName());

    private UriInfo uriInfo;
    private HttpHeaders httpHeaders;
    private Request request;
    private String resourceUri;

    public PeengResourceSrv(@Context javax.ws.rs.core.UriInfo uriInfo, @Context javax.ws.rs.core.HttpHeaders httpHeaders, @Context javax.ws.rs.core.Request request)
    {
        this.uriInfo = uriInfo;
        this.httpHeaders = httpHeaders;
        this.request = request;
        this.resourceUri = uriInfo.getAbsolutePath().toString();
    }


    @GET
    @Produces({MediaType.TEXT_PLAIN})
    public Response createUniqueId(@QueryParam("command") String command) throws BaseResourceException
    {
        // Arg, command, ignored for now.
        try {
            Long currentTime = System.currentTimeMillis();
            log.log(Level.INFO, "Ping srv called at " + currentTime);
            return Response.ok().entity(currentTime.toString()).build();
        //} catch(BadRequestException ex) {
        //    throw new BadRequestRsException(ex, resourceUri);
        //} catch(RequestForbiddenException ex) {
        //    throw new RequestForbiddenRsException(ex, resourceUri);
        //} catch(RequestConflictException ex) {
        //    throw new RequestConflictRsException(ex, resourceUri);
        //} catch(ServiceUnavailableException ex) {
        //    throw new ServiceUnavailableRsException(ex, resourceUri);
        //} catch(InternalServerErrorException ex) {
        //    throw new InternalServerErrorRsException(ex, resourceUri);
        //} catch(BaseException ex) {
        //    throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new BaseResourceException("Unknown error!", ex, resourceUri);
        }
    }

}
