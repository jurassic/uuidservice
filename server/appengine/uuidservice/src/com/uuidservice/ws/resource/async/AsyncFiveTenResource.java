package com.uuidservice.ws.resource.async;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;

import com.uuidservice.ws.BaseException;
import com.uuidservice.ws.CommonConstants;
import com.uuidservice.ws.core.GUID;
import com.uuidservice.ws.exception.BadRequestException;
import com.uuidservice.ws.exception.InternalServerErrorException;
import com.uuidservice.ws.exception.NotImplementedException;
import com.uuidservice.ws.exception.RequestConflictException;
import com.uuidservice.ws.exception.RequestForbiddenException;
import com.uuidservice.ws.exception.DataStoreException;
import com.uuidservice.ws.exception.ResourceGoneException;
import com.uuidservice.ws.exception.ResourceNotFoundException;
import com.uuidservice.ws.exception.ResourceAlreadyPresentException;
import com.uuidservice.ws.exception.ServiceUnavailableException;
import com.uuidservice.ws.exception.resource.BaseResourceException;
import com.uuidservice.ws.resource.exception.BadRequestRsException;
import com.uuidservice.ws.resource.exception.InternalServerErrorRsException;
import com.uuidservice.ws.resource.exception.NotImplementedRsException;
import com.uuidservice.ws.resource.exception.RequestConflictRsException;
import com.uuidservice.ws.resource.exception.RequestForbiddenRsException;
import com.uuidservice.ws.resource.exception.DataStoreRsException;
import com.uuidservice.ws.resource.exception.ResourceGoneRsException;
import com.uuidservice.ws.resource.exception.ResourceNotFoundRsException;
import com.uuidservice.ws.resource.exception.ResourceAlreadyPresentRsException;
import com.uuidservice.ws.resource.exception.ServiceUnavailableRsException;

import com.uuidservice.ws.FiveTen;
import com.uuidservice.ws.bean.FiveTenBean;
import com.uuidservice.ws.stub.FiveTenListStub;
import com.uuidservice.ws.stub.FiveTenStub;
import com.uuidservice.ws.resource.ServiceManager;
import com.uuidservice.ws.resource.FiveTenResource;


@Path("/_task/w/fiveTens/")
public class AsyncFiveTenResource extends BaseAsyncResource implements FiveTenResource
{
    private static final Logger log = Logger.getLogger(AsyncFiveTenResource.class.getName());

    private UriInfo uriInfo;
    private HttpHeaders httpHeaders;
    private Request request;
    private String resourceUri;
    private String queueName = null;
    private String taskName = null;
    private Integer retryCount = null;
    private boolean dummyPayload = false;

    public AsyncFiveTenResource(@Context javax.ws.rs.core.UriInfo uriInfo, @Context javax.ws.rs.core.HttpHeaders httpHeaders, @Context javax.ws.rs.core.Request request)
    {
        this.uriInfo = uriInfo;
        this.httpHeaders = httpHeaders;
        this.request = request;
        this.resourceUri = uriInfo.getAbsolutePath().toString();
        List<String> qns = httpHeaders.getRequestHeader("X-AppEngine-QueueName");
        if(qns != null && qns.size() > 0) {
            this.queueName = qns.get(0);
        }
        List<String> tns = httpHeaders.getRequestHeader("X-AppEngine-TaskName");
        if(tns != null && tns.size() > 0) {
            this.taskName = tns.get(0);
        }
        List<String> rcs = httpHeaders.getRequestHeader("X-AppEngine-TaskRetryCount");
        if(rcs != null && rcs.size() > 0) {
            String strCount = rcs.get(0);
            try {
                this.retryCount = Integer.parseInt(strCount);
            } catch(NumberFormatException ex) {
                // ignore.
                //this.retryCount = 0;
            }
        }
        List<String> ats = httpHeaders.getRequestHeader("X-AsyncTask-Payload");
        if(ats != null && ats.size() > 0 && ats.get(0).equals("DummyPayload")) {
            this.dummyPayload = true;
        }
    }

    private Response getFiveTenList(List<FiveTen> beans) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getAllFiveTens(String ordering, Long offset, Integer count) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response findFiveTens(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getFiveTen(String guid) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getFiveTen(String guid, String field) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response createFiveTen(FiveTenStub fiveTen) throws BaseResourceException
    {
        log.log(Level.INFO, "createFiveTen(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            FiveTenBean bean = null;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                FiveTenStub realStub = (FiveTenStub) getCache().get(taskName);
                if(realStub == null) {
                    throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                }
                log.fine("Real stub retrieved from memCache. realStub = " + realStub);
                bean = convertFiveTenStubToBean(realStub);
            } else {
                bean = convertFiveTenStubToBean(fiveTen);
            }
            String guid = ServiceManager.getFiveTenService().createFiveTen(bean);
            URI createdUri = URI.create(resourceUri + "/" + guid);
            log.log(Level.INFO, "createFiveTen(): Successfully processed the request: createdUri = " + createdUri.toString());
            return Response.created(createdUri).entity(guid).type(MediaType.TEXT_PLAIN).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ResourceAlreadyPresentException ex) {
            throw new ResourceAlreadyPresentRsException(ex, resourceUri);
        } catch(DataStoreException ex) {
            throw new DataStoreRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response updateFiveTen(String guid, FiveTenStub fiveTen) throws BaseResourceException
    {
        log.log(Level.INFO, "updateFiveTen(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            guid = GUID.normalize(guid);    // TBD: Validate guid?
            if(fiveTen == null || !guid.equals(fiveTen.getGuid())) {
                log.log(Level.WARNING, "Path param guid = " + guid + " is different from fiveTen guid = " + fiveTen.getGuid());
                throw new RequestForbiddenException("Failed to update the fiveTen with guid = " + guid);
            }
            FiveTenBean bean = null;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                FiveTenStub realStub = (FiveTenStub) getCache().get(taskName);
                if(realStub == null) {
                    throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                }
                log.fine("Real stub retrieved from memCache. realStub = " + realStub);
                bean = convertFiveTenStubToBean(realStub);
            } else {
                bean = convertFiveTenStubToBean(fiveTen);
            }
            boolean suc = ServiceManager.getFiveTenService().updateFiveTen(bean);
            if(suc == false) {
                log.log(Level.WARNING, "Failed to update the fiveTen with guid = " + guid);
                throw new InternalServerErrorException("Failed to update the fiveTen with guid = " + guid);
            }
            log.log(Level.INFO, "updateFiveTen(): Successfully processed the request: guid = " + guid);
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(DataStoreException ex) {
            throw new DataStoreRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response updateFiveTen(String guid, Integer counter, String requesterIpAddress) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response deleteFiveTen(String guid) throws BaseResourceException
    {
        log.log(Level.INFO, "deleteFiveTen(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            guid = GUID.normalize(guid);    // TBD: Validate guid?
            boolean suc = ServiceManager.getFiveTenService().deleteFiveTen(guid);
            if(suc == false) {
                log.log(Level.WARNING, "Failed to delete the fiveTen with guid = " + guid);
                throw new InternalServerErrorException("Failed to delete the fiveTen with guid = " + guid);
            }
            log.log(Level.INFO, "deleteFiveTen(): Successfully processed the request: guid = " + guid);
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(DataStoreException ex) {
            throw new DataStoreRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response deleteFiveTens(String filter, String params, List<String> values) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    public static FiveTenBean convertFiveTenStubToBean(FiveTen stub)
    {
        FiveTenBean bean = new FiveTenBean();
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Empty bean is returned.");
        } else {
            bean.setGuid(stub.getGuid());
            bean.setCounter(stub.getCounter());
            bean.setRequesterIpAddress(stub.getRequesterIpAddress());
            bean.setCreatedTime(stub.getCreatedTime());
            bean.setModifiedTime(stub.getModifiedTime());
        }
        return bean;
    }

}
