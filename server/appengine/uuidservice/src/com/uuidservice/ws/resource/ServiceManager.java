package com.uuidservice.ws.resource;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.uuidservice.ws.service.UniqueIdService;
import com.uuidservice.ws.service.impl.UniqueIdServiceImpl;
import com.uuidservice.ws.service.FiveTenService;
import com.uuidservice.ws.service.impl.FiveTenServiceImpl;

// TBD:
// Implement pooling, etc. ????
public final class ServiceManager
{
    private static final Logger log = Logger.getLogger(ServiceManager.class.getName());

	private static UniqueIdService uniqueIdService = null;
	private static FiveTenService fiveTenService = null;

    // Prevents instantiation.
    private ServiceManager() {}

    // Returns a UniqueIdService instance.
	public static UniqueIdService getUniqueIdService() 
    {
        if(ServiceManager.uniqueIdService == null) {
            ServiceManager.uniqueIdService = new UniqueIdServiceImpl();
        }
        return ServiceManager.uniqueIdService;
    }

    // Returns a FiveTenService instance.
	public static FiveTenService getFiveTenService() 
    {
        if(ServiceManager.fiveTenService == null) {
            ServiceManager.fiveTenService = new FiveTenServiceImpl();
        }
        return ServiceManager.fiveTenService;
    }

}
