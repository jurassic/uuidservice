package com.uuidservice.ws.resource.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.uuidservice.ws.core.StatusCode;
import com.uuidservice.ws.stub.ErrorStub;

@Provider
public class RequestTimeoutExceptionMapper implements ExceptionMapper<RequestTimeoutRsException>
{
    public Response toResponse(RequestTimeoutRsException ex) {
        return Response.status(Status.fromStatusCode(StatusCode.REQUEST_TIMEOUT))
        .entity(new ErrorStub(ex))
        //.type(MediaType.TEXT_PLAIN)
        .build();
    }

}
