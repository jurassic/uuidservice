package com.uuidservice.ws.resource;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.uuidservice.ws.BaseException;
import com.uuidservice.ws.exception.resource.BaseResourceException;
import com.uuidservice.ws.FiveTen;
import com.uuidservice.ws.stub.FiveTenStub;


// TBD: Partial update/overwrite?
// TBD: Field-based filtering in getFiveTen(guid). (e.g., ?field1=x&field2=y)
public interface FiveTenResource
{
    @GET
    @Path("all")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response getAllFiveTens(@QueryParam("ordering") String ordering, @QueryParam("offset") Long offset, @QueryParam("count") Integer count) throws BaseResourceException;

    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response findFiveTens(@QueryParam("filter") String filter, @QueryParam("ordering") String ordering, @QueryParam("params") String params, @QueryParam("values") List<String> values, @QueryParam("grouping") String grouping, @QueryParam("unique") Boolean unique, @QueryParam("offset") Long offset, @QueryParam("count") Integer count) throws BaseResourceException;

    @GET
    @Path("count")
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response getCount(@QueryParam("filter") String filter, @QueryParam("params") String params, @QueryParam("values") List<String> values, @QueryParam("aggregate") String aggregate) throws BaseResourceException;

    @GET
    @Path("{guid : [0-9a-fA-F\\-]+}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response getFiveTen(@PathParam("guid") String guid) throws BaseResourceException;

    @GET
    @Path("{guid : [0-9a-fA-F\\-]+}/{field : [a-zA-Z_][0-9a-zA-Z_]*}")
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response getFiveTen(@PathParam("guid") String guid, @PathParam("field") String field) throws BaseResourceException;

    @POST
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response createFiveTen(FiveTenStub fiveTen) throws BaseResourceException;

    @PUT
    @Path("{guid : [0-9a-fA-F\\-]+}")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response updateFiveTen(@PathParam("guid") String guid, FiveTenStub fiveTen) throws BaseResourceException;

    //@PUT  ???
    @POST   // We can adhere to semantics of PUT=Replace, POST=update. PUT is supported in HTML form in HTML5 only.
    @Path("{guid : [0-9a-fA-F\\-]+}")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response updateFiveTen(@PathParam("guid") String guid, @QueryParam("counter") Integer counter, @QueryParam("requesterIpAddress") String requesterIpAddress) throws BaseResourceException;

    @DELETE
    @Path("{guid : [0-9a-fA-F\\-]+}")
    Response deleteFiveTen(@PathParam("guid") String guid) throws BaseResourceException;

    @DELETE
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response deleteFiveTens(@QueryParam("filter") String filter, @QueryParam("params") String params, @QueryParam("values") List<String> values) throws BaseResourceException;

}
